'use strict';

// Services and Factories have their first letter capitalized like Controllers

module.exports = angular.module('common.services', [])
  .service('request', require('./request.js'))
  .factory('api', require('./api.js'));
