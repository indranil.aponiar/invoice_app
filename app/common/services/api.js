'use strict';
module.exports = [function() {
  var baseurl = "/api/";
  var api = {
    data: {
      'invoices': 'invoices',
      'products': 'products',
      'customers': 'customers',
      'invoiceItem': function(id) {
        return 'invoices/' + id + '/items'
      }
    }
  };
  api.getApi = function(key, params) {
    if (typeof api.data[key] === 'function') {
      return baseurl + api.data[key](params);
    } else if (api.data[key] && api.data[key].static) {
      return api.data[key].path;
    } else {
      return baseurl + api.data[key];
    }

  };
  return api;
}];
