'use strict';
module.exports = [
  '$http',
  '$q',
  function($http, $q) {
    this.doRequest = function(config) {
      var deffered = $q.defer();
      var requestObject = {
        url: config.url,
        headers: {}
      };
      if (config.method) {
        requestObject.method = config.method;
      } else {
        requestObject.method = 'GET'
      }
      if (requestObject.method != 'GET' && config.data) {
        requestObject.data = config.data;

      }
      if (config.headers) {
        angular.extend(requestObject.headers, config.headers);
      }
      if (requestObject.method === 'GET' && config.data) {
        requestObject.url += '?' + $.param(config.data);
      }
      // $http returns a promise, which has a then function, which also returns a promise
      $http(requestObject).success(function(response) {
          deffered.resolve(response);
        })
        .error(function(error) {
          deffered.reject(error);
        });
      // Return the promise to the controller
      return deffered.promise;
    }
  }
]
