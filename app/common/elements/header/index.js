'use strict';

module.exports = angular.module('common.elements.commonHeader', [])
  .directive('commonHeader', function() {
    return {
      template: require('./common-header.html'),
      restrict: 'EA',
      replace: true,
      controller: function($rootScope, $scope) {
        $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
          if (toState && toState.name) {
            $scope.activeMenu = toState.name;
          }
        });
      }
    };
  });
