window.jQuery = window.$ = require('jquery');
window._ = require('lodash');

require('angular-bootstrap');
require('angular-ui-router');
require('angular-animate');
require('angular-sanitize');
require('domready/ready');
require('lodash');

module.exports = angular.module('common', [
  'ui.bootstrap',
  'ui.router',
  'ngAnimate',
  'ngSanitize',
  require('./elements/header').name,
  require('./services').name
]);
