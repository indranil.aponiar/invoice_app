'use strict';
// Home View
module.exports = angular.module('modules.invoice', [])
  .controller('InvoiceCtrl', require('./controllers/InvoiceController'))
  .controller('EditInvoiceCtrl', require('./controllers/EditInvoiceController'))
  .factory('InvoiceList', require('./factories/InvoiceData'))
  .config(require('./invoiceRoutes'));
