'use strict';
// Controller naming conventions should start with an uppercase letter
function InvoiceCtrl($scope, $state, InvoiceList, CustomerList, api, request, $timeout) {
    var me = this;
    angular.extend(me, {
        init: function() {
            me.initScope();
        },
        initScope: function() {
            me.loadCustomers();
        },
        loadCustomers: function() {
            me.isLoading = true;
            CustomerList.getData().then(function(response) {
                me.customers = response;
                me.loadInvoices();
            }, function(error) {
                delete me.isLoading;
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            })
        },
        loadInvoices: function() {
            me.isLoading = true;
            InvoiceList.getData().then(function(response) {
                me.invoices = response;
                delete me.isLoading;
            }, function(error) {
                delete me.isLoading;
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            })
        },
        getCustomerName: function(customerId) {
            var name = '';
            angular.forEach(me.customers, function(customer) {
                if (customer.id == customerId) {
                    name = customer.name;
                }
            })
            return name;
        },
        getCustomerAddress: function(customerId) {
            var address = '';
            angular.forEach(me.customers, function(customer) {
                if (customer.id == customerId) {
                    address = customer.address;
                }
            })
            return address;
        },
        openEditInvice: function(invoiceId) {
            $state.go('edit-invoice', {
                'id': invoiceId
            })
        },
        removeInvice: function(index) {
            if (me.invoices && me.invoices[index] && me.invoices[index].id)
                var removeId = me.invoices[index].id;
            request.doRequest({
                'url': api.getApi('invoices') + '/' + removeId,
                'method': 'DELETE'
            }).then(function(response) {
                me.invoices.forEach(function(invoice, itemIndex) {
                    if (invoice.id == removeId) {
                        me.invoices.splice(itemIndex, 1);
                        InvoiceList.setData(me.invoices);
                    }
                })
            }, function(error) {
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            })
        }
    }).init();
}

// $inject is necessary for minification. See http://bit.ly/1lNICde for explanation.
InvoiceCtrl.$inject = ['$scope', '$state', 'InvoiceList', 'CustomerList', 'api', 'request', '$timeout'];
module.exports = InvoiceCtrl;