'use strict';
// Controller naming conventions should start with an uppercase letter
function AddInvoiceCtrl($scope, $state, $stateParams, CustomerList, ProductList, InvoiceList, $timeout, request, api) {
    var me = this;
    angular.extend(me, {
        init: function() {
            me.initScope();
        },
        initScope: function() {
            me.loadCustomers();
        },
        loadCustomers: function() {
            me.isLoading = true;
            CustomerList.getData().then(function(response) {
                me.customers = response;
                me.loadProducts();
            }, function(error) {
                delete me.isLoading;
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            })
        },
        loadProducts: function() {
            ProductList.getData().then(function(response) {
                me.products = response;
                if ($stateParams.id) {
                    me.loadInvoice();
                } else {
                    delete me.isLoading;
                    me.invoceData = {
                        'customer_id': '',
                        'products': []
                    };
                }
            }, function(error) {
                delete me.isLoading;
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            })
        },
        loadInvoice: function() {
            request.doRequest({
                'url': api.getApi('invoices') + '/' + $stateParams.id,
                'method': 'GET'
            }).then(function(response) {
                me.invoceData = response;
                me.loadInvoiceItems();
            }, function(error) {
                delete me.isLoading;
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            })
        },
        loadInvoiceItems: function() {
            request.doRequest({
                'url': api.getApi('invoices') + '/' + $stateParams.id + "/items",
                'method': 'GET'
            }).then(function(response) {
                delete me.isLoading;
                me.invoceData.products = response;

            }, function(error) {
                delete me.isLoading;
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            })
        },
        addProducts: function() {
            me.invoceData.products.push({
                'product_id': '',
                'quantity': 0
            })
        },
        removeProduct: function(index) {
            if (me.invoceData.products[index]) {
                if (me.invoceData.products[index].id) {
                    me.removeInvoiceItem(me.invoceData, me.invoceData.products[index]);
                }
                me.invoceData.products.splice(index, 1);
            }
        },
        getTotal: function() {
            var total = 0;
            angular.forEach(me.invoceData.products, function(addedProduct) {
                if (addedProduct.product_id && addedProduct.quantity) {
                    angular.forEach(me.products, function(product) {
                        if (product.id == addedProduct.product_id) {
                            total += (product.price * addedProduct.quantity)
                        }
                    })
                }
            })
            if (me.invoceData.discount && me.invoceData.discount > 0) {
                total = total - (total * me.invoceData.discount * 0.01);
            }
            if (total > 0) {
                me.invoceData.total = total.toFixed(2);
                return me.invoceData.total;
            } else {
                return 0;
            }
        },
        saveInvoice: function() {
            if(!me.invoceData.customer_id) 
                return;
            var url, method;
            if (me.invoceData.id) {
                url = api.getApi('invoices') + '/' + me.invoceData.id;
                method = 'PUT';
            } else {
                url = api.getApi('invoices');
                method = 'POST';
            }
            me.isSaving = true;
            me.saveSuccess = false;
            me.saveFailed = false;
            request.doRequest({
                'url': url,
                'method': method,
                'data': me.invoceData
            }).then(function(response) {
                me.saveSuccess = true;
                InvoiceList.getData().then(function(invoices) {
                    if (me.invoceData.id) {
                        if (invoices && invoices.length) {
                            invoices.forEach(function(invoice, index) {
                                if (invoice.id == response.id) {
                                    invoices[index] = angular.copy(response);
                                    InvoiceList.setData(invoices);
                                }
                            })
                        }
                    } else {
                        if (!invoices || !invoices.length) {
                            invoices = [];
                        }
                        invoices.push(response);
                        InvoiceList.setData(invoices);
                    }
                    angular.extend(me.invoceData, response);
                })
            }, function(error) {
                me.saveFailed = true
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            }).finally(function(){
                console.log('complete');
                me.isSaving = false;
            })
        },

        editProduct: function(index) {
            if (me.invoceData.products[index]) {
                if (me.invoceData.products[index].product_id && me.invoceData.products[index].quantity) {
                    me.addInvoiceItem(me.invoceData, me.invoceData.products[index]);
                }
            }
        },
        addInvoiceItem: function(invoice, product) {
            var url, method;
            if (product.id) {
                url = api.getApi('invoiceItem', invoice.id) + '/' + product.id;
                method = 'PUT';
            } else {
                url = api.getApi('invoiceItem', invoice.id);
                method = 'POST';
            }
            request.doRequest({
                'url': url,
                'method': method,
                'data': {
                    'invoice_id': invoice.id,
                    'product_id': product.product_id,
                    'quantity': product.quantity
                }
            }).then(function(response) {
                me.saveInvoice();
                angular.extend(product, response);
            }, function(error) {
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            })
        },
        removeInvoiceItem: function(invoice, product) {
            request.doRequest({
                'url': api.getApi('invoiceItem', invoice.id) + '/' + product.id,
                'method': 'DELETE'
            }).then(function(response) {
                me.saveInvoice();
            }, function(error) {
                if (error.message) {
                    me.errorMsg = error.message;
                } else {
                    me.errorMsg = "Something went wrong! Please try again";
                }
                $timeout(function() {
                    delete me.errorMsg;
                }, 5000);
            })
        }
    }).init();
}

// $inject is necessary for minification. See http://bit.ly/1lNICde for explanation.
AddInvoiceCtrl.$inject = ['$scope', '$state', '$stateParams', 'CustomerList', 'ProductList', 'InvoiceList', '$timeout', 'request', 'api'];
module.exports = AddInvoiceCtrl;