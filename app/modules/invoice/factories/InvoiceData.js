'use strict';
module.exports = [
  '$q',
  'request',
  'api',
  function($q, request, api) {
    var invoiceList = {
      data: null
    };
    invoiceList.setData = function(data) {
      invoiceList.data = data;
    };
    invoiceList.resetData = function(data) {
      invoiceList.data = null;
    };
    invoiceList.getData = function() {
      var deffered = $q.defer();
      if (invoiceList.data) {
        deffered.resolve(invoiceList.data);
      } else {
        request.doRequest({
          'url': api.getApi('invoices')
        }).then(function(response) {

          invoiceList.data = response;

          deffered.resolve(invoiceList.data);

        }, function(error) {
          deffered.reject(error);
        })
      }
      return deffered.promise;
    };
    return invoiceList;
  }
]
