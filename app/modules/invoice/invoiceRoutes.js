'use strict';

function invoiceRoutes($stateProvider) {

  $stateProvider.state({
    name: 'invoice', // state name
    url: '/invoice', // url path that activates this state
    template: require('./templates/invoice.html'), // generate the Directive "homeView" - when calling the directive in HTML, the name must not be camelCased
    controller: 'InvoiceCtrl',
    controllerAs: 'invoiceCtrl',
    data: {
      moduleClasses: 'page', // assign a module class to the <body> tag
      pageClasses: 'invoice', // assign a page-specific class to the <body> tag
      pageTitle: 'Invoice list', // set the title in the <head> section of the index.html file
      pageDescription: 'Invoice list' // meta description in <head>
    }
  });
  $stateProvider.state({
    name: 'add-invoice', // state name
    url: '/addinvoice', // url path that activates this state
    template: require('./templates/editinvoice.html'), // generate the Directive "homeView" - when calling the directive in HTML, the name must not be camelCased
    controller: 'EditInvoiceCtrl',
    controllerAs: 'editInvoiceCtrl',
    data: {
      moduleClasses: 'page', // assign a module class to the <body> tag
      pageClasses: 'add invoice', // assign a page-specific class to the <body> tag
      pageTitle: 'Add Invoice', // set the title in the <head> section of the index.html file
      pageDescription: 'Add Invoice' // meta description in <head>
    }
  });
  $stateProvider.state({
    name: 'edit-invoice', // state name
    url: '/invoice/:id', // url path that activates this state
    template: require('./templates/editinvoice.html'), // generate the Directive "homeView" - when calling the directive in HTML, the name must not be camelCased
    controller: 'EditInvoiceCtrl',
    controllerAs: 'editInvoiceCtrl',
    data: {
      moduleClasses: 'page', // assign a module class to the <body> tag
      pageClasses: 'edit invoice', // assign a page-specific class to the <body> tag
      pageTitle: 'Edit Invoice', // set the title in the <head> section of the index.html file
      pageDescription: 'Edit Invoice' // meta description in <head>
    }
  });

}

invoiceRoutes.$inject = ['$stateProvider'];
module.exports = invoiceRoutes;
