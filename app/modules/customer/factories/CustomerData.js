'use strict';
module.exports = [
  '$q',
  'request',
  'api',
  function($q, request, api) {
    var customerList = {
      data: null
    };
    customerList.setData = function(data) {
      customerList.data = data;
    };
    customerList.resetData = function(data) {
      customerList.data = null;
    };
    customerList.getData = function() {
      var deffered = $q.defer();
      if (customerList.data) {
        deffered.resolve(customerList.data);
      } else {
        request.doRequest({
          'url': api.getApi('customers')
        }).then(function(response) {

          customerList.data = response;

          deffered.resolve(customerList.data);

        }, function(error) {
          deffered.reject(error);
        })
      }
      return deffered.promise;
    };
    return customerList;
  }
]
