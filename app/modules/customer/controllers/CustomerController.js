'use strict';
// Controller naming conventions should start with an uppercase letter
function CustomerCtrl($scope, CustomerList, $timeout) {
  var me = this;
  angular.extend(me, {
    init: function() {
      me.initScope();
    },
    initScope: function() {
      me.loadCustomers();
    },
    loadCustomers: function() {
      me.isLoading = true;
      CustomerList.getData().then(function(response) {
        me.customers = response;
        delete me.isLoading;
      }, function(error) {
        delete me.isLoading;
        if (error.message) {
          me.errorMsg = error.message;
        } else {
          me.errorMsg = "Something went wrong! Please try again";
        }
        $timeout(function() {
          delete me.errorMsg;
        }, 5000);
      })
    }
  }).init();
}

// $inject is necessary for minification. See http://bit.ly/1lNICde for explanation.
CustomerCtrl.$inject = ['$scope', 'CustomerList', '$timeout'];
module.exports = CustomerCtrl;
