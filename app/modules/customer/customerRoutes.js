'use strict';

function customerRoutes($stateProvider) {
  $stateProvider.state({
    name: 'customer', // state name
    url: '/customer', // url path that activates this state
    template: require('./templates/customer.html'), // generate the Directive "homeView" - when calling the directive in HTML, the name must not be camelCased
    controller: 'CustomerCtrl',
    controllerAs: 'customerCtrl',
    data: {
      moduleClasses: 'page', // assign a module class to the <body> tag
      pageClasses: 'customer', // assign a page-specific class to the <body> tag
      pageTitle: 'Customer list', // set the title in the <head> section of the index.html file
      pageDescription: 'Customer list' // meta description in <head>
    }
  });

}

customerRoutes.$inject = ['$stateProvider'];
module.exports = customerRoutes;
