'use strict';
// Home View
module.exports = angular.module('modules.customer', [])
  .controller('CustomerCtrl', require('./controllers/CustomerController'))
  .factory('CustomerList', require('./factories/CustomerData'))
  .config(require('./customerRoutes'));
