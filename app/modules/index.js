'use strict';

module.exports = angular.module('modules', [
  require('./invoice').name,
  require('./product').name,
  require('./customer').name
]);
