'use strict';
module.exports = [
  '$q',
  'request',
  'api',
  function($q, request, api) {
    var productList = {
      data: null
    };
    productList.setData = function(data) {
      productList.data = data;
    };
    productList.resetData = function(data) {
      productList.data = null;
    };
    productList.getData = function() {
      var deffered = $q.defer();
      if (productList.data) {
        deffered.resolve(productList.data);
      } else {
        request.doRequest({
          'url': api.getApi('products')
        }).then(function(response) {

          productList.data = response;

          deffered.resolve(productList.data);

        }, function(error) {
          deffered.reject(error);
        })
      }
      return deffered.promise;
    };
    return productList;
  }
]
