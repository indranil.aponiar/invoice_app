'use strict';
// Controller naming conventions should start with an uppercase letter
function ProductCtrl($scope, ProductList, $timeout) {
  var me = this;
  angular.extend(me, {
    init: function() {
      me.initScope();
    },
    initScope: function() {
      me.loadProducts();
    },
    loadProducts: function() {
      me.isLoading = true;
      ProductList.getData().then(function(response) {
        me.products = response;
        delete me.isLoading;
      }, function(error) {
        delete me.isLoading;
        if (error.message) {
          me.errorMsg = error.message;
        } else {
          me.errorMsg = "Something went wrong! Please try again";
        }
        $timeout(function() {
          delete me.errorMsg;
        }, 5000);
      })
    }
  }).init();
}

// $inject is necessary for minification. See http://bit.ly/1lNICde for explanation.
ProductCtrl.$inject = ['$scope', 'ProductList', '$timeout'];
module.exports = ProductCtrl;
