'use strict';
// Home View
module.exports = angular.module('modules.product', [])
  .controller('ProductCtrl', require('./controllers/ProductController'))
  .factory('ProductList', require('./factories/ProductData'))
  .config(require('./productRoutes'));
