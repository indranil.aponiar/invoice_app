'use strict';

function productRoutes($stateProvider) {

  $stateProvider.state({
    name: 'product', // state name
    url: '/product', // url path that activates this state
    template: require('./templates/product.html'), // generate the Directive "homeView" - when calling the directive in HTML, the name must not be camelCased
    controller: 'ProductCtrl',
    controllerAs: 'productCtrl',
    data: {
      moduleClasses: 'page', // assign a module class to the <body> tag
      pageClasses: 'product', // assign a page-specific class to the <body> tag
      pageTitle: 'Product list', // set the title in the <head> section of the index.html file
      pageDescription: 'Product list' // meta description in <head>
    }
  });

}

productRoutes.$inject = ['$stateProvider'];
module.exports = productRoutes;
